<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Car;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CarFactory extends Factory
{

    protected $model = Car::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'brand' => $this->faker->randomElement(['BMW','Audi','VW','Seat','Mercedes','Honda','Toyota']),
            'model' => $this->faker->word(),
            'year'   => $this->faker->year(),
            'max_speed'=> $this->faker->numberBetween(150, 290),
            'number_of_doors'=> $this->faker->randomNumber(1, true),
            'is_automatic'=> $this->faker->boolean(10),
            'engine'=> $this->faker->randomElement(['diesel','petrol','electric','hybrid'])
        ];

    }
}
